# Reseni
## Databazovy model 
![DB model](/db_model.png/)

## Navrh webove aplikace
- https://www.figma.com/file/bTa4XqmG0gTQVDN8GPBR08/Untitled?node-id=3%3A3


# Zadani
## Téma semestrální práce
Naimplementujte webovou aplikaci na téma „Rezervační systém učeben“. Umožňuje vybraným uživatelům rezervovat vybrané skupiny učeben. Této rezervaci předchází schválení. Naimplementujte RESTové rozhraní pro práci s rezervačním systémem, především rozhraní pro otevírání, odemykání a zamykání dveří.

## Nefunkční požadavky
- N.1 použití frameworku pro tvorbu webových aplikací na straně serveru (na úrovni Symfony).
- N.2 použití pokročilé knihovny pro práci s databází (na úrovni Doctrine).
- N.3 validní a sémantické XHTML nebo HTML 5 (preferované)
- N.4 validní CSS (LESS, SASS, …​) a originální styl stránky (nechceme další Bootstrap, ideálně i varianta pro tisk)
- N.5 Implementace RESTApi (příp. GraphQL)
- N.6 JavaScript + AJAX pro dynamické chování
- N.7 přístupná varianta webu

## Role
- R.1 Nepřihlášený uživatel
	- může prohlížet obsazenost veřejných místností
	- nemá přístup k „soukromým“ učebnám
	- nemůže vykonávat žádnou akci
- R.2 Přihlášený uživatel (může navíc)
	- spravuje svoje žádosti (nemůže je sám zakládat, měnit datum a místnost)
	- R.2.1 Uživatel místnosti
		- může rezervovat místnosti jejichž je uživatelem (např. kancelář, Apple učebna, …​)
		- žádost musí schválit správce místnosti nebo správce skupiny
	- R.2.2 Správce místnosti (může navíc oproti uživateli místnosti)
		- může spravovat a schvalovat rezervace místností, jejichž je správce (např. síťová účebna, SAGELab, …​)
		- může je rezervovat i pro jiné uživatele
	- R.2.3 Člen skupiny
		- může rezervovat místnosti patřící dané skupině (např. KTI, KAM, KSI, …​)
		- žádost musí schválit správce místnosti nebo správce skupiny
	- R.2.4 Správce skupiny (může navíc oproti správci místnosti)
		- může spravovat a schvalovat rezervace místností, které patří dané skupině (i tranzitivně podskupinám) jejíchž je správce (např. vedoucí katedry)
		- může je rezervovat i pro jiné uživatele
		- může nastavit (vytvořit, změnit, zrušit) správce konkrétní místnosti patřící dané skupině (i tranzitivně)
- R.3 Administrátor
	- má kompletní přístup
	- může vytvářet nové skupiny a podskupiny, jmenovat jejich správce, přesouvat učebny mezi těmito skupinami
	- může vytvářet nové místnosti

## Funkční požadavky

### Aplikace
- Správce uživatelů (v rozsahu Projektu na cvičení, můžete využít vámi napsaný kód či jeho části)
- Správce skupin [CRUD]
- Správce místností a budov [CRUD]
- Žádosti a rezervace [CRUD]

### RESTApi (příp. GraphQL)
- Uživatelé (seznam, filtrování, detail)
- Skupiny (seznam, filtrování)
	- Přidávání a odebírání uživatelů
	- Přidávání a odebírání místností
- Místnosti (seznam, filtrování, detail)
	- Odemykání a zamykání místnosti (rezervovaná místnost dvojitým přiložením karty aktuálního vlastníka se odemkne nebo naopak se zamkne)

- Rezervace (seznam, filtrování, detail)
	- kompletní CRUD (vč. schvalování a přidávání "návštěvníků" místnosti během rezervace)
	- ověření zda uživatel má v tuto chvíli přístup do místnosti (je neobsazená, je jejím uživatelem, má schválenou rezervaci)
	
### JavaScript & AJAX
Využijte části RESTového API (příp. GraphQL)
- Použijte vlastní definici objektů (nestačí pouze procedurální kód)
- Uživatelé
	- zobrazení náhledu uživatelského profilu při najetí na uživatelské jméno
- Skupiny
	- přidávání a odebírání uživatelů bez nutnosti přenačtení stránky
	- přidávání a odebírání místností bez nutnosti přenačtení stránky
- Rezervace
	- přidávání a odebírání „návštěvníků“ rezervace bez nutnosti přenačtení stránky
	- dynamické filtrování seznamu pro přidání a odebrání

